var express = require('express');
var router = express.Router();
var programsData = require('../../controllers/program.controller');

/* Program POST creation */
router.post('/', function(req, res, next) {
    programsData.createProgram(req.body, function (response) {
      res.json(response);
    });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var programsData = require('../../controllers/program.controller');

/* GET users listing. */
router.get('/', function(req, res, next) {
    programsData.getAllPrograms(function (response) {
       res.json(response);
    });
});

module.exports = router;

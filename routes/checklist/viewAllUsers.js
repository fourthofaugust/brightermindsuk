var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/checklist.controller');

/* GET users listing. */
router.get('/', function (req, res, next) {
    usersData.getAllUsers(function (response) {
        res.json(response);
    });
});

module.exports = router;

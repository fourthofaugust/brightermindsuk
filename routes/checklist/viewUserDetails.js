var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/checklist.controller');

/* user POST creation */
router.post('/', function (req, res, next) {
    console.log(req)
    usersData.getUserDetails(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

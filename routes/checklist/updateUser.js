var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/checklist.controller');

/* Program POST creation */
router.post('/', function (req, res, next) {
    usersData.updateUserDetails(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/users.controller');

/* user POST creation */
router.post('/', function (req, res, next) {
    usersData.getUserDetails(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

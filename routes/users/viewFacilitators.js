var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/users.controller');

/* GET facilitators listing. */
router.get('/', function (req, res, next) {
    usersData.getAllFacilitators(function (response) {
        res.json(response);
    });
});

module.exports = router;

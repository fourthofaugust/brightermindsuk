var express = require('express');
var router = express.Router();
var usersData = require('../../controllers/users.controller');

/* GET trainers listing. */
router.get('/', function (req, res, next) {
    usersData.getAllTrainers(function (response) {
        res.json(response);
    });
});

module.exports = router;

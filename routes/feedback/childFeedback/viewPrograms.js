var express = require('express');
var router = express.Router();
var progressData = require('../../../controllers/childProgress.controller');

/* GET users listing. */
router.get('/', function (req, res, next) {
    progressData.getAllProgress(function (response) {
        res.json(response);
    });
});

module.exports = router;

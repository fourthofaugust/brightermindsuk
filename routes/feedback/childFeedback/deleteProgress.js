var express = require('express');
var router = express.Router();
var progressData = require('../../../controllers/childProgress.controller');

/* Program POST creation */
router.post('/', function (req, res, next) {
    progressData.deleteProgramDetails(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

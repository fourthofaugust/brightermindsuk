var express = require('express');
var router = express.Router();
var progressData = require('../../../controllers/childProgress.controller');

/* Progress POST creation */
router.post('/', function (req, res, next) {
    progressData.createProgress(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

var express = require('express');
var router = express.Router();
var feedbackData = require('../../../controllers/parentsFeedback.controller');

/* GET users listing. */
router.get('/', function (req, res, next) {
    feedbackData.getAllFeedbacks(function (response) {
        res.json(response);
    });
});

module.exports = router;

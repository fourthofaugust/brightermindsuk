var express = require('express');
var router = express.Router();
var feedbackData = require('../../../controllers/parentsFeedback.controller');

/* Program POST creation */
router.post('/', function (req, res, next) {
    feedbackData.getFeedbackDetails(req.body, function (response) {
        res.json(response);
    });
});

module.exports = router;

var config = require('../config/dbConfig');
var db = require("mongo-util");
var collectionName = "feedbackToParent";
db.init(config.getMongoConnectionUrl());

exports.getFeedbacksData = function (callback) {
    db.findAll(collectionName, {}, function (err, data) {
        callback(err, data);
    });
};

exports.getFeedbackDetails = function (docId, callback) {
    db.find(collectionName, {
        _id: docId
    }, function (err, data) {
        callback(err, data);
    });
};

exports.createFeedback = function (callback) {
    db.insertOne(collectionName, feedbackData, function (err, data) {
        callback(err, data);
    });
};

exports.updateFeedbackData = function (selector, feedbackData, callback) {

    db.updateOne(collectionName, selector, feedbackData, function (err, result) {
        callback(err, result);
    });
};

exports.deleteFeedbackData = function (selector, feedbackData, callback) {

    db.deleteOne(collectionName, feedbackData, function (err, result) {
        callback(err, result);
    });
};


module.exports = exports;
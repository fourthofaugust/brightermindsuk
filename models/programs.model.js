var config = require('../config/dbConfig');
var db = require("mongo-util");
var collectionName="programs";
db.init(config.getMongoConnectionUrl());

exports.getProgramsData=function(callback) {
    db.findAll(collectionName, function (err, data) {
       callback(err,data);
    });
};

exports.getProgramDetails=function(docId,callback){
    console.log(docId);
    db.find(collectionName, {
        _id: docId
    }, function(err, data) {
        console.log(data);
        callback(err,data);
    });
};

exports.createProgram = function (programData, callback) {
    db.insertOne(collectionName, programData, function (err, data) {
        callback(err,data);
    });
};

exports.updateProgramData=function (selector,programData,callback) {

    db.updateOne(collectionName, selector, programData, function (err, result) {
       callback(err,result);
    });
};

exports.deleteProgramData = function (programData, callback) {

    db.deleteOne(collectionName, programData.program_name, function (err, result) {
        if (result.result.n) {
            result = 1;
        }
        else {
            result = 0;
        }
        callback(err,result);
    });
};


module.exports=exports;
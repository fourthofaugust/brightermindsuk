var config = require('../config/dbConfig');
var db = require("mongo-util");
var collectionName = "checklist";
db.init(config.getMongoConnectionUrl());

exports.getUsersData = function (callback) {
    db.findAll(collectionName, function (err, data) {
        callback(err, data);
    });
};

exports.getFacilitatorsData = function (callback) {
    db.find(collectionName, {'user_type': 'facilitators'}, function (err, data) {
        callback(err, data);
    });
};

exports.getTrainersData = function (callback) {
    db.find(collectionName, {'user_type': 'trainer'}, function (err, data) {
        callback(err, data);
    });
};

exports.getUserDetails = function (docId, callback) {
    console.log(docId);
    db.find(collectionName, {
        _id: docId
    }, function (err, data) {
        console.log(data);
        callback(err, data);
    });
};


exports.createUser = function (userData, callback) {
    db.insertOne(collectionName, userData, function (err, data) {
        callback(err, data);
    });
};

exports.updateUserData = function (selector, userData, callback) {

    db.updateOne(collectionName, selector, userData, function (err, result) {
        callback(err, result);
    });
};

exports.deleteUserData = function (userData, callback) {

    db.deleteOne(collectionName, userData.user_name, function (err, result) {
        if (result.result.n) {
            result = 1;
        }
        else {
            result = 0;
        }
        callback(err, result);
    });
};


module.exports = exports;
var config = require('../config/dbConfig');
var db = require("mongo-util");
var collectionName = "childProgress";
db.init(config.getMongoConnectionUrl());

exports.getProgressData = function (callback) {
    db.findAll(collectionName, {}, function (err, data) {
        callback(err, data);
    });
};

exports.getProgressDetails = function (docId, callback) {
    db.find(collectionName, {
        _id: docId
    }, function (err, data) {
        callback(err, data);
    });
};

exports.createProgress = function (callback) {
    db.insertOne(collectionName, programData, function (err, data) {
        callback(err, data);
    });
};

exports.updateProgressData = function (selector, programData, callback) {

    db.updateOne(collectionName, selector, programData, function (err, result) {
        callback(err, result);
    });
};

exports.deleteProgressData = function (selector, programData, callback) {

    db.deleteOne(collectionName, programData, function (err, result) {
        callback(err, result);
    });
};


module.exports = exports;
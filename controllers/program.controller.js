var ProgramDB=require("../models/programs.model");

exports.getAllPrograms = function(callback) {
    ProgramDB.getProgramsData(function(err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {

            if (!data.length) {
                var json = {
                    "error": "Sorry no results found"
                }
            }else{
                var json = {
                    "success": data
                }
            }
            callback(json);
            console.log(data.length)
        }
    });
};

exports.getProgramDetails = function (doc, callback) {

    ProgramDB.getProgramDetails(doc.docId, function (err, data) {
        console.log(data);
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            if (data.length) {
                var json = {
                    "success": data[0]
                }
            }
            else {
                var json = {
                    "error": "Sorry program not found. Please recheck and try again"
                }
            }

            callback(json);
        }
    });
};

checkProgramExist = function(username, callback) {

    ProgramDB.getProgramDetails(username, function (err, data) {
        if (err) {
            callback("error", null);
        } else {
            if (data.length) {
                callback(null, 1);
            } else {
                callback(null, 0);
            }
        }
    });
};

exports.createProgram = function(programData, callback) {
    checkProgramExist(programData.program_name, function(err, response) {
        if (err) {
            var json = {
                "error": "Sorry something went wrong"
            };
            callback(json);
        } else {
            if (response.length) {
                var json = {
                    "error": "Sorry program already registered"
                };
                callback(json);
            } else {
                programData._id = programData.program_name;

                ProgramDB.createProgram(programData,function (err,data) {
                    if (err) {
                        var json = {
                            "error": "Sorry program already registered"
                        };
                        callback(json);
                    } else {
                        var json = {
                            "success": "Succesfully created program"
                        };
                        callback(json);
                    }
                });
            }
        }
    });
};

exports.updateProgramDetails = function(programData, callback) {

    var query = {
        _id: programData.program_name
    };

    ProgramDB.updateProgramData(query,programData,function(err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating program data failed"
            };
        } else {
            var json = {
                "success": "Program data updated successfully"
            };
        }
        callback(json);
    });
};

exports.deleteProgramDetails = function(programData, callback) {

    ProgramDB.deleteProgramData(programData,function(err, success) {

        if (err) {
            var json = {
                "error": "Sorry deleting program failed. Please try again"
            };
        } else {

            if (success) {
                var json = {
                    "success": "Program data deleted successfully"
                };
            }
            else {
                var json = {
                    "success": "Sorry deleting program failed. Please try again"
                };
            }
        }
        callback(json);
    });
};


module.exports = exports;

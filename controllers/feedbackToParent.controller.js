var feedbackDB = require("../models/feedbackToParent.schema");

exports.getAllFeedbacks = function (callback) {
    feedbackDB.getFeedbacksData(function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            if (data.length > 0) {
                var json = {
                    "error": "Sorry no results found"
                }
            } else {
                var json = {
                    "success": data
                }
            }
            callback(json);
        }
    });
};

exports.getFeedbackDetails = function (docId, callback) {

    feedbackDB.getFeedbackDetails(docId, function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            var json = {
                "success": data
            };
            callback(json);
        }
    });
};

exports.createFeedback = function (feedbackData, callback) {
    feedbackDB.createFeedback(feedbackData, function (err, data) {
        if (err) {
            var json = {
                "error": err
            };
            callback(json);
        } else {
            var json = {
                "success": "Succesfully created feedback"
            };
            callback(json);
        }
    });
};

exports.updateFeedbackDetails = function (feedbackData, callback) {

    var query = {
        _id: feedbackData.feedback_name
    };

    feedbackDB.updateFeedbackData(query, feedbackData, function (err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating feedback data failed"
            };
        } else {
            var json = {
                "success": "Feedback data updated successfully"
            };
        }
        callback(json);
    });
};

exports.deleteFeedbackDetails = function (feedbackData, callback) {

    feedbackDB.deleteFeedbackData(feedbackData, function (err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating feedback data failed"
            };
        } else {
            var json = {
                "success": "Feedback data updated successfully"
            };
        }
        callback(json);
    });
};


module.exports = exports;

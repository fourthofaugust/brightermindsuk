var UsersDB = require("../models/users.schema");

exports.getAllUsers = function (callback) {
    UsersDB.getUsersData(function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {

            if (!data.length) {
                var json = {
                    "error": "Sorry no results found"
                }
            } else {
                var json = {
                    "success": data
                }
            }
            callback(json);
            console.log(data.length)
        }
    });
};

exports.getAllFacilitators = function (callback) {
    UsersDB.getFacilitatorsData(function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {

            if (!data.length) {
                var json = {
                    "error": "Sorry no results found"
                }
            } else {
                var json = {
                    "success": data
                }
            }
            callback(json);
            console.log(data.length)
        }
    });
};

exports.getAllTrainers = function (callback) {
    UsersDB.getTrainersData(function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {

            if (!data.length) {
                var json = {
                    "error": "Sorry no results found"
                }
            } else {
                var json = {
                    "success": data
                }
            }
            callback(json);
            console.log(data.length)
        }
    });
};

exports.getUserDetails = function (doc, callback) {

    UsersDB.getUserDetails(doc.docId, function (err, data) {
        console.log(data);
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            if (data.length) {
                var json = {
                    "success": data[0]
                }
            }
            else {
                var json = {
                    "error": "Sorry user not found. Please recheck and try again"
                }
            }

            callback(json);
        }
    });
};


exports.login = function (doc, callback) {

    UsersDB.getUserDetails(doc.email, function (err, data) {
        console.log(data);
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            if (data.length) {
                if (data[0].password == doc.password) {
                    var json = {
                        "success": "User logged in successfully",
                        "access": data[0].access
                    }
                }
                else {
                    var json = {
                        "error": "Sorry login failed. Please recheck and try again"
                    }
                }

            }
            else {
                var json = {
                    "error": "Sorry login failed. Please recheck and try again"
                }
            }

            callback(json);
        }
    });
};

checkUserExist = function (username, callback) {

    UsersDB.getUserDetails(username, function (err, data) {
        if (err) {
            callback("error", null);
        } else {
            if (data.length) {
                callback(null, 1);
            } else {
                callback(null, 0);
            }
        }
    });
};

exports.createUser = function (userData, callback) {
    checkUserExist(userData.user_name, function (err, response) {
        if (err) {
            var json = {
                "error": "Sorry something went wrong"
            };
            callback(json);
        } else {
            if (response.length) {
                var json = {
                    "error": "Sorry user already registered"
                };
                callback(json);
            } else {
                userData._id = userData.user_name;

                UsersDB.createUser(userData, function (err, data) {
                    if (err) {
                        var json = {
                            "error": "Sorry user already registered"
                        };
                        callback(json);
                    } else {
                        var json = {
                            "success": "Succesfully created user"
                        };
                        callback(json);
                    }
                });
            }
        }
    });
};

exports.updateUserDetails = function (userData, callback) {

    var query = {
        _id: userData.user_name
    };

    UsersDB.updateUserData(query, userData, function (err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating user data failed"
            };
        } else {
            var json = {
                "success": "User data updated successfully"
            };
        }
        callback(json);
    });
};

exports.deleteUserDetails = function (userData, callback) {

    UsersDB.deleteUserData(userData, function (err, success) {

        if (err) {
            var json = {
                "error": "Sorry deleting user failed. Please try again"
            };
        } else {

            if (success) {
                var json = {
                    "success": "User data deleted successfully"
                };
            }
            else {
                var json = {
                    "success": "Sorry deleting user failed. Please try again"
                };
            }
        }
        callback(json);
    });
};


module.exports = exports;

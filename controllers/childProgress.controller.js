var ProgressDB = require("../models/childProgress.schema");

exports.getAllProgress = function (callback) {
    ProgressDB.getProgressData(function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            if (data.length > 0) {
                var json = {
                    "error": "Sorry no results found"
                }
            } else {
                var json = {
                    "success": data
                }
            }
            callback(json);
        }
    });
};

exports.getProgressDetails = function (docId, callback) {

    ProgressDB.getProgressDetails(docId, function (err, data) {
        if (err) {
            var json = {
                "error": "Something went wrong while retrieving"
            };
            callback(json);
        } else {
            var json = {
                "success": data
            };
            callback(json);
        }
    });
};

exports.createProgress = function (feedbackData, callback) {
    ProgressDB.createProgress(feedbackData, function (err, data) {
        if (err) {
            var json = {
                "error": err
            };
            callback(json);
        } else {
            var json = {
                "success": "Succesfully created feedback"
            };
            callback(json);
        }
    });
};

exports.updateProgressDetails = function (feedbackData, callback) {

    var query = {
        _id: feedbackData.feedback_name
    };

    ProgressDB.updateProgressData(query, feedbackData, function (err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating feedback data failed"
            };
        } else {
            var json = {
                "success": "Progress data updated successfully"
            };
        }
        callback(json);
    });
};

exports.deleteProgressDetails = function (feedbackData, callback) {

    ProgressDB.deleteProgressData(feedbackData, function (err, success) {
        if (err) {
            var json = {
                "error": "Sorry updating feedback data failed"
            };
        } else {
            var json = {
                "success": "Progress data updated successfully"
            };
        }
        callback(json);
    });
};


module.exports = exports;

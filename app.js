var express = require('express');
var app = express();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var db = require("mongo-util");
var http = require('http');
var server = require('./bin/www');

var debug = require('debug')('brighterminds:server');
var http = require('http');

/* importing db configuration details for db operations */
var config = require('./config/dbConfig'); // importing database configuration

/* routes */
var index = require('./routes/index');
var users = require('./routes/users');

/* program routes initializing*/
var createProgram = require('./routes/programs/createProgram');
var viewProgramDetails = require('./routes/programs/viewProgramDetails');
var viewAllPrograms = require('./routes/programs/viewPrograms');
var deleteProgram = require('./routes/programs/deleteProgram');
var updateProgram = require('./routes/programs/updateProgram');

/* User routes initializing*/
var createUser = require('./routes/Users/createUser');
var viewUserDetails = require('./routes/Users/viewUserDetails');
var viewAllUsers = require('./routes/Users/viewAllUsers');
var viewAllTrainers = require('./routes/Users/viewTrainers');
var viewAllFacilitators = require('./routes/Users/viewFacilitators');
var deleteUser = require('./routes/Users/deleteUser');
var updateUser = require('./routes/Users/updateUser');
var userLogin = require('./routes/Users/login');

/*check routes initializing*/
/* Checklist routes initializing*/
var createChecklist = require('./routes/checklist/createUser');
var viewChecklistDetails = require('./routes/checklist/viewUserDetails');
var viewAllChecklists = require('./routes/checklist/viewAllUsers');
var deleteChecklist = require('./routes/checklist/deleteUser');
var updateChecklist = require('./routes/checklist/updateUser');

/* child progress routes initializing*/
var createFeedback = require('./routes/feedback/childFeedback/createProgress');
var viewFeedbackDetails = require('./routes/feedback/childFeedback/viewProgressDetails');
var viewAllFeedbacks = require('./routes/feedback/childFeedback/viewPrograms');
var deleteFeedback = require('./routes/feedback/childFeedback/deleteProgress');
var updateFeedback = require('./routes/feedback/childFeedback/updateProgress');

/* feedback to the parent routes initializing*/
var createFeedbackToParent = require('./routes/feedback/feedbackToParent/createFeedback');
var viewFeedbackToParentDetails = require('./routes/feedback/feedbackToParent/viewFeedbackDetails');
var viewAllFeedbacksToParent = require('./routes/feedback/feedbackToParent/viewFeedbacks');
var deleteFeedbackToParent = require('./routes/feedback/feedbackToParent/deleteFeedback');
var updateFeedbackToParent = require('./routes/feedback/feedbackToParent/updateFeedback');

/* parents feedback routes initializing*/
var createParentsFeedback = require('./routes/feedback/parentsFeedback/createFeedback');
var viewParentsFeedbackDetails = require('./routes/feedback/parentsFeedback/viewFeedbackDetails');
var viewAllParentsFeedback = require('./routes/feedback/parentsFeedback/viewFeedbacks');
var deleteParentsFeedback = require('./routes/feedback/parentsFeedback/deleteFeedback');
var updateParentsFeedback = require('./routes/feedback/parentsFeedback/updateFeedback');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

/* programs handling routes */
app.use('/login', userLogin);
app.use('/createProgram',createProgram);
app.use('/viewProgramDetails',viewProgramDetails);
app.use('/viewPrograms', viewAllPrograms);
app.use('/deleteProgram',deleteProgram);
app.use('/updateProgram', updateProgram);

/* Users handling routes */
app.use('/createUser', createUser);
app.use('/viewUserDetails', viewUserDetails);
app.use('/viewUsers', viewAllUsers);
app.use('/viewFacilitators', viewAllFacilitators);
app.use('/viewTrainers', viewAllTrainers);
app.use('/deleteUser', deleteUser);
app.use('/updateUser', updateUser);

/* Checklists handling routes */
app.use('/createChecklist', createChecklist);
app.use('/viewChecklistDetails', viewChecklistDetails);
app.use('/viewChecklists', viewAllChecklists);
app.use('/updateChecklist', updateChecklist);

/*  child progress handling routes */
app.use('/createChildProgressFeedback', createFeedback);
app.use('/viewChildProgressFeedbackDetails', viewFeedbackDetails);
app.use('/viewChildProgressFeedbacks', viewAllFeedbacks);
app.use('/deleteChildProgressFeedback', deleteFeedback);
app.use('/updateChildProgressFeedback', updateFeedback);

/*  feedback to parent handling routes */
app.use('/createFeedbackToParent', createFeedbackToParent);
app.use('/viewFeedbackToParentDetails', viewFeedbackToParentDetails);
app.use('/viewFeedbackToParent', viewAllFeedbacksToParent);
app.use('/deleteFeedbackToParent', deleteFeedbackToParent);
app.use('/updateFeedbackToParent', updateFeedbackToParent);

/*  parents feedback handling routes */
app.use('/createparentsFeedback', createParentsFeedback);
app.use('/viewparentsFeedbackDetails', viewParentsFeedbackDetails);
app.use('/viewparentsFeedback', viewAllParentsFeedback);
app.use('/deleteparentsFeedback', deleteParentsFeedback);
app.use('/updateparentsFeedback', updateParentsFeedback);

app.get('/email/inbox', function (req, res) {
    res.send("email")
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

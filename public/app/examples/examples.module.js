(function () {
    'use strict';

    angular
        .module('app.brighterminds', [
            'app.brighterminds.authentication',
            'app.brighterminds.programs',
            'app.brighterminds.settings',
            'app.brighterminds.cms',
            'app.brighterminds.programs'
        ]);
})();
